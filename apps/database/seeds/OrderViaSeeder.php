<?php

use Illuminate\Database\Seeder;
use App\OrderViaModel;

class OrderViaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderViaModel::create
		([
			'name' => "Whats App",
			'delete' => 0
		]);

		OrderViaModel::create
		([
			'name' => "BBM",
			'delete' => 0
		]);

		OrderViaModel::create
		([
			'name' => "Line",
			'delete' => 0
		]);
    }
}
