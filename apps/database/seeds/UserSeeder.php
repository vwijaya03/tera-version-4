<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create
		([
			'fullname' => "Viko Wijaya",
			'email' => "viko_wijaya@yahoo.co.id",
			'username' => "viko",
			'password' => Hash::make("viko"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "superadmin",
			'delete' => 0
		]);

		UserModel::create
		([
			'fullname' => "Lina",
			'email' => "lina@mail.com",
			'username' => "lina",
			'password' => Hash::make("lina"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "superadmin",
			'delete' => 0
		]);

		UserModel::create
		([
			'fullname' => "hendy",
			'email' => "hendy@mail.com",
			'username' => "lina",
			'password' => Hash::make("hendy"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "superadmin",
			'delete' => 0
		]);

		UserModel::create
		([
			'fullname' => "gudang",
			'email' => "gudang@mail.com",
			'username' => "lina",
			'password' => Hash::make("gudang"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "gudang",
			'delete' => 0
		]);

		UserModel::create
		([
			'fullname' => "admin",
			'email' => "admin@mail.com",
			'username' => "lina",
			'password' => Hash::make("admin"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "admin",
			'delete' => 0
		]);

		UserModel::create
		([
			'fullname' => "kasir",
			'email' => "kasir@mail.com",
			'username' => "lina",
			'password' => Hash::make("kasir"),
			'telp' => "12345678910",
			'alamat' => "Surabaya",
			'jabatan' => "kasir",
			'delete' => 0
		]);
    }
}
