<?php

use Illuminate\Database\Seeder;
use App\KategoriModel;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriModel::create
		([
			'name' => "Baut",
			'delete' => 0
		]);

		KategoriModel::create
		([
			'name' => "Ban",
			'delete' => 0
		]);

		KategoriModel::create
		([
			'name' => "Bor",
			'delete' => 0
		]);

		KategoriModel::create
		([
			'name' => "Tang",
			'delete' => 0
		]);
    }
}
