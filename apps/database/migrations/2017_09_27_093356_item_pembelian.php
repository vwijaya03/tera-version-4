<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pembelian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('pembelian_id');
            $table->string('item_po_id');
            $table->string('harga_satuan');
            $table->string('quantity');
            $table->string('diskon');
            $table->string('delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_pembelian');
    }
}
