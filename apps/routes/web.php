<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/anime', 'DashboardController@getAnime')->name('getAnime');
Route::get('/detail_anime', 'DashboardController@getDetailAnime')->name('getDetailAnime');
Route::get('/recent_release', 'DashboardController@getRecentReleaseAnime')->name('getRecentReleaseAnime');

Route::get('/', 'LoginController@getLogin')->name('getLogin'); 
Route::get('/login', 'LoginController@getLogin')->name('getLogin'); 
Route::post('/login', 'LoginController@postLogin')->name('postLogin'); 
Route::get('/logout', 'LoginController@getLogout')->name('getLogout'); 
Route::get('/dashboard', 'DashboardController@getDashboard')->name('getDashboard');

// Kategori
Route::get('/kategori', 'KategoriController@getKategori')->name('getKategori');
Route::post('/kategori-ajax', 'KategoriController@postAjaxGetKategori')->name('postAjaxGetKategori');
Route::get('/add-kategori', 'KategoriController@getAddKategori')->name('getAddKategori');
Route::post('/add-kategori', 'KategoriController@postAddKategori')->name('postAddKategori');
Route::get('/edit-kategori/{id}', 'KategoriController@getEditKategori')->name('getEditKategori');
Route::post('/edit-kategori/{id}', 'KategoriController@postEditKategori')->name('postEditKategori');
Route::post('/delete-kategori/{id}', 'KategoriController@postDeleteKategori')->name('postDeleteKategori');

// Barang
Route::get('/barang', 'BarangController@getBarang')->name('getBarang');
Route::post('/barang-ajax', 'BarangController@postAjaxGetBarang')->name('postAjaxGetBarang');
Route::get('/add-barang', 'BarangController@getAddBarang')->name('getAddBarang');
Route::post('/add-barang', 'BarangController@postAddBarang')->name('postAddBarang');
Route::get('/edit-barang/{id}', 'BarangController@getEditBarang')->name('getEditBarang');
Route::post('/edit-barang/{id}', 'BarangController@postEditBarang')->name('postEditBarang');
Route::post('/delete-barang/{id}', 'BarangController@postDeleteBarang')->name('postDeleteBarang');

// Stok Barang
Route::get('/stok-barang', 'BarangController@getStokBarang')->name('getStokBarang');
Route::post('/stok-barang-ajax', 'BarangController@postAjaxGetStokBarang')->name('postAjaxGetStokBarang');

// Order Via
Route::get('/order-via', 'OrderViaController@getOrderVia')->name('getOrderVia');
Route::post('/order-via-ajax', 'OrderViaController@postAjaxGetOrderVia')->name('postAjaxGetOrderVia');
Route::get('/add-order-via', 'OrderViaController@getAddOrderVia')->name('getAddOrderVia');
Route::post('/add-order-via', 'OrderViaController@postAddOrderVia')->name('postAddOrderVia');
Route::get('/edit-order-via/{id}', 'OrderViaController@getEditOrderVia')->name('getEditOrderVia');
Route::post('/edit-order-via/{id}', 'OrderViaController@postEditOrderVia')->name('postEditOrderVia');
Route::post('/delete-order-via/{id}', 'OrderViaController@postDeleteOrderVia')->name('postDeleteOrderVia');

// Sales
Route::get('/sales', 'SalesController@getSales')->name('getSales');
Route::get('/search-sales', 'SalesController@getSearchSales')->name('getSearchSales');
Route::post('/sales-ajax', 'SalesController@postAjaxGetSales')->name('postAjaxGetSales');
Route::get('/add-sales', 'SalesController@getAddSales')->name('getAddSales');
Route::post('/add-sales', 'SalesController@postAddSales')->name('postAddSales');
Route::get('/edit-sales/{id}', 'SalesController@getEditSales')->name('getEditSales');
Route::post('/edit-sales/{id}', 'SalesController@postEditSales')->name('postEditSales');
Route::post('/delete-sales/{id}', 'SalesController@postDeleteSales')->name('postDeleteSales');

// Supplier
Route::get('/supplier', 'SupplierController@getSupplier')->name('getSupplier');
Route::get('/search-supplier', 'SupplierController@getSearchSupplier')->name('getSearchSupplier');
Route::post('/supplier-ajax', 'SupplierController@postAjaxGetSupplier')->name('postAjaxGetSupplier');
Route::get('/add-supplier', 'SupplierController@getAddSupplier')->name('getAddSupplier');
Route::post('/add-supplier', 'SupplierController@postAddSupplier')->name('postAddSupplier');
Route::get('/edit-supplier/{id}', 'SupplierController@getEditSupplier')->name('getEditSupplier');
Route::post('/edit-supplier/{id}', 'SupplierController@postEditSupplier')->name('postEditSupplier');
Route::post('/delete-supplier/{id}', 'SupplierController@postDeleteSupplier')->name('postDeleteSupplier');

// PO
Route::get('/po', 'POController@getPO')->name('getPO');
Route::post('/po', 'POController@postPO')->name('postPO');





