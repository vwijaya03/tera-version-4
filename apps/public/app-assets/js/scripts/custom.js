$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function master_edit(url)
{
    var execute_url = url;

    window.location.href = execute_url;
}

function master_delete(url, master_data)
{
    swal({
        title: "Apakah anda yakin ?",
        text: "Data tidak bisa di kembalikan !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var execute_url = url;

            $.ajax({
                type: "POST",
                url: execute_url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Berhasil Di Hapus',
                        text: 'Data berhasil di hapus!',
                        type: 'success'
                    }, function() {
                        window.location.href = master_data;
                    });
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}