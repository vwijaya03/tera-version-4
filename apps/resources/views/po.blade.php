@extends('header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Purchase Order</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Purchase Order
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Purchase Order</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									@if ($errors->has('kategori'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Kategori</strong> belum ada yang di pilih !
										</div>
									@endif

									@if ($errors->has('sku'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>SKU</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('name'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Nama Barang</strong> tidak boleh kosong !
										</div>
									@endif

									<form action="{{ url('/po') }}" method="POST" class="form">

										{!! csrf_field() !!}

										<h4 class="form-section"><i class="ft-file-text"></i> Form Purchase Order</h4>

											<div class="row">
												<div class="col-xl-4 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Month &amp; Year Selectors</label>
														<div class="input-group">
															<input style="background-color: white !important;" type='text' class="form-control pickadate-selectors" placeholder="Month &amp; Year Selector"/>
															<span class="input-group-addon">
																<span class="fa fa-calendar-o"></span>
															</span>
														</div>
														<small class="text-muted">Use <code>.pickadate-selectors</code> class to set month &amp; year selectable.</small>
													</div>
												</div>

												<div class="col-xl-4 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Vendor</label>
														<select class="select2-data-ajax-supplier form-control" id="select2-ajax" name="supplier"></select>
													</div>
												</div>

												<div class="col-xl-4 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Sales</label>
														<select class="select2-data-ajax-sales form-control" id="select2-ajax" name="sales"></select>
													</div>
												</div>
											</div>

										<div class="form-actions">
											<a href="{{ url('/barang') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('po_ajax_url')

<script type="text/javascript">
	var supplier_url = "{{ $supplier_url }}";
	var sales_url = "{{ $sales_url }}";
</script>
<script src="{{ URL::asset('app-assets/js/scripts/forms/select/form-select2.min.js') }}" type="text/javascript"></script>

@endsection