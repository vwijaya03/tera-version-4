@extends('header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data Order Via</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Master Data</a>
                            </li>
                            <li class="breadcrumb-item active">Master Order Via
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">
				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data Order Via</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="{{ url('/add-order-via') }}" class="btn btn-success mr-1 mb-1" target="_blank">Tambah Data Baru</a>
									<br><br>
									<table class="table table-striped table-bordered dataex-html5-export server-side-order-via">
										<thead>
											<tr>
												<th>Id</th>
												<th>Name</th>
												<th></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Id</th>
												<th>Name</th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
	$(document).ready(function() {

	    $('.server-side-order-via').DataTable({
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url('/order-via-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "id" },
	            { "data": "name" },
	            { "data": "btn" }
	        ]	 

	    });
	});
</script>

@endsection