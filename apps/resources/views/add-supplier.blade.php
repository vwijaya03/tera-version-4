@extends('header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data Supplier</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Master Data</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/supplier') }}">Master Supplier</a>
                            </li>
                            <li class="breadcrumb-item active">Tambah Data Supplier Baru
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Tambah Data Supplier Baru</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									@if ($errors->has('fullname'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Nama Lengkap Supplier</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('email'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Email</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('telp'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>No. Telepon</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('alamat'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Alamat</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('termin'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Termin</strong> tidak boleh kosong !
										</div>
									@endif

									<form action="{{ url('/add-supplier') }}" method="POST" class="form">

										{!! csrf_field() !!}

										<div class="form-body">
											<h4 class="form-section"><i class="ft-file-text"></i> Tambah Data Supplier Baru</h4>
											<div class="row">
												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Nama Lengkap Supplier</label>
														<input type="text" class="form-control" placeholder="Nama Lengkap Supplier" name="fullname" value="{{ old('fullname') }}">
													</div>
												</div>

												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Email</label>
														<input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>No. Telepon</label>
														<input type="text" class="form-control" placeholder="No. Telepon" name="telp" value="{{ old('telp') }}">
													</div>
												</div>

												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Alamat</label>
														<input type="text" class="form-control" placeholder="Alamat" name="alamat" value="{{ old('alamat') }}">
													</div>
												</div>

												<div class="col-xl-4 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Termin</label>
														<input type="number" min="0" class="form-control" placeholder="Termin" name="termin" value="{{ old('termin') }}">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url('/supplier') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection