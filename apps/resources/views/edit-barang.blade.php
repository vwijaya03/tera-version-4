@extends('header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Ubah Data Barang</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Master Data</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/barang') }}">Master Barang</a>
                            </li>
                            <li class="breadcrumb-item active">Ubah Data Barang
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Ubah Data Barang</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									
									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif

									@if ($errors->has('kategori'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Kategori</strong> belum ada yang di pilih !
										</div>
									@endif

									@if ($errors->has('sku'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>SKU</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('name'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Nama Barang</strong> tidak boleh kosong !
										</div>
									@endif

									<form action="{{ url('/edit-barang/'.$barang->barang_id) }}" method="POST" class="form">

										{!! csrf_field() !!}

										<div class="form-body">
											<h4 class="form-section"><i class="ft-file-text"></i> Ubah Data Barang</h4>

											<div class="row">
												<div class="col-xl-4 col-lg-6 col-md-12">
					                                <fieldset class="form-group">
					                                    <label for="customSelect">Pilih Kategori</label>
					                                    <select class="custom-select block" id="customSelect" name="kategori">
					                                        <option value="">Pilih Kategori</option>
					                                        
					                                        <option selected value="{{ $selected_category->id }}">{{ $selected_category->name }}</option>

					                                        @foreach($unselected_categories as $unselected_category)
					                                        <option value="{{ $unselected_category->id }}">{{ $unselected_category->name }}</option>
					                                        @endforeach
					                                    </select>
					                                </fieldset>                                
					                            </div>
											</div>
											
											<div class="row">
												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>SKU</label>
														<input type="text" class="form-control always-show-maxlength" id="maxlength-always-show" maxlength="25" placeholder="SKU" name="sku" value="{{ $barang->barang_sku }}" autocomplete="off">
													</div>
												</div>

												<div class="col-xl-6 col-lg-6 col-md-12">
													<div class="form-group">
														<label>Nama Barang</label>
														<input type="text" class="form-control" placeholder="Nama Barang" name="name" value="{{ $barang->barang_name }}">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url('/barang') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>

											<a href="{{ url('/add-barang') }}" class="btn btn-success mr-1"><i class="ft-file-text"></i> Tambah Data Barang Lagi</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection