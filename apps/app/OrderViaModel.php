<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class OrderViaModel extends Model
{
    protected $table = 'order_via';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'delete'
    ];

    private $success_update_msg = 'Data berhasil di ubah.';
	private $success_add_msg = 'Data berhasil di tambahkan.';
	private $success_delete_msg = 'Data berhasil di hapus.';

	public function countAllActiveOrderVia()
    {
        $count_order_via = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_order_via;
    }

    public function countAllFilteredActiveOrderVia($search)
    {
        $count_order_via = $this->select('id')
        ->where(function ($q) use($search) {
        	$q->where('id', 'like', '%'.$search.'%');
        	$q->orWhere('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_order_via;
    }

    public function getOrderVia($start, $limit, $order, $dir)
    {
    	$order_via = $this->select('id', 'name')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $order_via;
    }

    public function getFilteredOrderVia($search, $start, $limit, $order, $dir)
    {
        $order_via = $this->select('id', 'name')
        ->where(function ($q) use($search) {
        	$q->where('id', 'like', '%'.$search.'%');
        	$q->orWhere('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $order_via;
    }

    public function getOneOrderVia($id)
    {
    	$order_via = $this->select('id', 'name')->where('id', $id)->where('delete', 0)->first();
    	return $order_via;
    }

    public function postAddOrderVia($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'name' => $param['name'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditOrderVia($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'name' => $param['name']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteOrderVia($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
