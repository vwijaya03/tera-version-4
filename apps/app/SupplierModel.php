<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class SupplierModel extends Model
{
    protected $table = 'supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'fullname', 'email', 'telp', 'alamat', 'termin', 'delete'
    ];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveSupplier()
    {
        $count_supplier = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_supplier;
    }

    public function countAllFilteredActiveSupplier($search)
    {
        $count_supplier = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
            $q->orWhere('termin', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_supplier;
    }

    public function getSupplier($start, $limit, $order, $dir)
    {
    	$supplier = $this->select('id', 'fullname', 'email', 'telp', 'alamat', 'termin')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $supplier;
    }

    public function getFilteredSupplier($search, $start, $limit, $order, $dir)
    {
        $supplier = $this->select('id', 'fullname', 'email', 'telp', 'alamat', 'termin')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
            $q->orWhere('termin', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $supplier;
    }

    public function getFilteredSupplierForSelect2($search)
    {
        $supplier = $this->select('id', 'fullname', 'email', 'telp', 'alamat', 'termin')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
            $q->orWhere('termin', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->paginate(30);

        return $supplier;
    }

    public function getOneSupplier($id)
    {
    	$supplier = $this->select('id', 'fullname', 'email', 'telp', 'alamat', 'termin')->where('id', $id)->where('delete', 0)->first();
    	return $supplier;
    }

    public function postAddSupplier($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'fullname' => $param['fullname'],
		    	'email' => $param['email'],
		    	'telp' => $param['telp'],
		    	'alamat' => $param['alamat'],
		    	'termin' => $param['termin'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditSupplier($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'fullname' => $param['fullname'],
		    	'email' => $param['email'],
		    	'telp' => $param['telp'],
		    	'alamat' => $param['alamat'],
		    	'termin' => $param['termin']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteSupplier($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
