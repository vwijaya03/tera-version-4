<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpItemPurchaseOrderModel extends Model
{
    protected $table = 'tmp_item_purchase_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'purchase_order_id', 'barang_id', 'harga_satuan', 'quantity', 'diskon', 'delete'
    ];
}
