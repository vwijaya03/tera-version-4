<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class SalesModel extends Model
{
    protected $table = 'sales';
    protected $primaryKey = 'id';
    protected $fillable = [
        'fullname', 'telp', 'alamat', 'delete'
    ];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveSales()
    {
        $count_sales = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_sales;
    }

    public function countAllFilteredActiveSales($search)
    {
        $count_sales = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_sales;
    }

    public function getSales($start, $limit, $order, $dir)
    {
    	$sales = $this->select('id', 'fullname', 'telp', 'alamat')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $sales;
    }

    public function getFilteredSales($search, $start, $limit, $order, $dir)
    {
        $sales = $this->select('id', 'fullname', 'telp', 'alamat')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $sales;
    }

    public function getFilteredSalesForSelect2($search)
    {
        $sales = $this->select('id', 'fullname', 'telp', 'alamat')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('fullname', 'like', '%'.$search.'%');
            $q->orWhere('telp', 'like', '%'.$search.'%');
            $q->orWhere('alamat', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->paginate(30);

        return $sales;
    }

    public function getOneSales($id)
    {
    	$sales = $this->select('id', 'fullname', 'telp', 'alamat')->where('id', $id)->where('delete', 0)->first();
    	return $sales;
    }

    public function postAddSales($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'fullname' => $param['fullname'],
		    	'telp' => $param['telp'],
		    	'alamat' => $param['alamat'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditSales($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'fullname' => $param['fullname'],
		    	'telp' => $param['telp'],
		    	'alamat' => $param['alamat']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteSales($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
