<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KategoriModel;
use Auth, Hash, DB, Log;

class BarangModel extends Model
{
	private $success_update_msg = 'Data berhasil di ubah.';
	private $success_add_msg = 'Data berhasil di tambahkan.';
	private $success_delete_msg = 'Data berhasil di hapus.';

	protected $table = 'barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kategori_id', 'sku', 'name', 'delete'
    ];

    public function countAllActiveBarang()
    {
        $count_barang = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_barang;
    }

    public function countAllFilteredActiveBarang($search)
    {
        $count_barang = $this->select('kategori.name as kategori_name', 'barang.id as barang_id', 'barang.sku as barang_sku', 'barang.name as barang_name')
        ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where(function ($q) use($search) {
            $q->where('barang.id', 'like', '%'.$search.'%'); 
            $q->orWhere('barang.name', 'like', '%'.$search.'%');
            $q->orWhere('barang.sku', 'like', '%'.$search.'%');
            $q->orWhere('kategori.name', 'like', '%'.$search.'%');
        })
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->count();

        return $count_barang;
    }

    public function getBarang($start, $limit, $order, $dir)
    {
    	$barang = $this->select('kategori.name as kategori_name', 'barang.id as barang_id', 'barang.sku as barang_sku', 'barang.name as barang_name')
    	->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $barang;
    }

    public function getFilteredBarang($search, $start, $limit, $order, $dir)
    {
        $barang = $this->select('kategori.name as kategori_name', 'barang.id as barang_id', 'barang.sku as barang_sku', 'barang.name as barang_name')
        ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where(function ($q) use($search) {
            $q->where('barang.id', 'like', '%'.$search.'%'); 
            $q->orWhere('barang.name', 'like', '%'.$search.'%');
            $q->orWhere('barang.sku', 'like', '%'.$search.'%');
            $q->orWhere('kategori.name', 'like', '%'.$search.'%');
        })
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $barang;
    }

    public function getOneBarang($id)
    {
    	$barang = $this->select('kategori.name as kategori_name', 'barang.id as barang_id', 'barang.kategori_id as barang_kategori_id', 'barang.sku as barang_sku', 'barang.name as barang_name')
    	->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
    	->where('barang.id', $id)
    	->where('barang.delete', 0)
    	->where('kategori.delete', 0)
    	->first();

    	return $barang;
    }

    public function getAllKategori()
    {
    	$kategori = KategoriModel::select('id', 'name')->where('delete', 0)->orderBy('name', 'asc')->get();

    	return $kategori;
    }

    public function getSelectedKategori($kategori_barang_id)
    {
    	$selected_kategori = KategoriModel::select('id', 'name')->where('id', $kategori_barang_id)->where('delete', 0)->first();

    	return $selected_kategori;
    }

    public function getUnSelectedKategori($kategori_barang_id)
    {
    	$unselected_kategori = KategoriModel::select('id', 'name')->where('id', '!=', $kategori_barang_id)->where('delete', 0)->orderBy('name', 'asc')->get();

    	return $unselected_kategori;
    }

    public function postAddBarang($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'kategori_id' => $param['kategori'],
		    	'sku' => $param['sku'],
		    	'name' => $param['name'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditBarang($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'kategori_id' => $param['kategori'],
		    	'sku' => $param['sku'],
		    	'name' => $param['name'],
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteBarang($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }

}
