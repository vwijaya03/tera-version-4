<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpItemPembelianModel extends Model
{
    protected $table = 'tmp_item_pembelian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'pembelian_id', 'sku', 'name', 'harga_satuan', 'quantity', 'diskon', 'delete'
    ];
}
