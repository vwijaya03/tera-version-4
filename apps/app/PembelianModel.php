<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembelianModel extends Model
{
    protected $table = 'pembelian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_invoice', 'supplier_id', 'tipe_pembayaran', 'extra_diskon', 'total', 'keterangan', 'validate', 'delete', 'tanggal_dibuat'
    ];
}
