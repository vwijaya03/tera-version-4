<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log, Feeds;

class KategoriModel extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'delete'
    ];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveKategori()
    {
        $count_kategori = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_kategori;
    }

    public function countAllFilteredActiveKategori($search)
    {
        $count_kategori = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_kategori;
    }

    public function getKategori($start, $limit, $order, $dir)
    {
    	$kategori = $this->select('id', 'name', 'created_at', 'updated_at')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $kategori;
    }

    public function getFilteredKategori($search, $start, $limit, $order, $dir)
    {
        $kategori = $this->select('id', 'name', 'created_at', 'updated_at')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $kategori;
    }

    public function getOneKategori($id)
    {
    	$kategori = $this->select('id', 'name')->where('id', $id)->where('delete', 0)->first();
    	return $kategori;
    }

    public function postAddKategori($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'name' => $param['name'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditKategori($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'name' => $param['name']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteKategori($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
