<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderModel extends Model
{
    protected $table = 'purchase_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sales_id', 'supplier_id', 'order_via_id', 'extra_diskon', 'total', 'keterangan', 'status', 'delete', 'tanggal_dibuat'
    ];

}
