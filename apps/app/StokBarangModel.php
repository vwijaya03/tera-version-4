<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class StokBarangModel extends Model
{
    protected $table = 'stok_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'barang_id', 'quantity', 'harga', 'delete'
    ];

    public function countAllActiveStokBarang()
    {
        $count_supplier = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_supplier;
    }

    public function countAllFilteredActiveStokBarang($search)
    {
        $count_supplier = $this->select('kategori.name', 'barang.name', 'barang.sku', 'stok_barang.id', 'stok_barang.quantity', 'stok_barang.harga')
        ->join('barang', 'barang.id', '=', 'stok_barang.barang_id')
        ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where(function ($q) use($search) {
            $q->where('stok_barang.id', 'like', '%'.$search.'%');
            $q->orWhere('stok_barang.quantity', 'like', '%'.$search.'%');
            $q->orWhere('stok_barang.harga', 'like', '%'.$search.'%');
            $q->orWhere('barang.name', 'like', '%'.$search.'%');
            $q->orWhere('barang.sku', 'like', '%'.$search.'%');
            $q->orWhere('kategori.name', 'like', '%'.$search.'%');
        })
        ->where('stok_barang.delete', 0)
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->count();

        return $count_supplier;
    }

    public function getStokBarang($start, $limit, $order, $dir)
    {
    	$supplier = $this->select('kategori.name as kategori_name', 'barang.name', 'barang.sku', 'stok_barang.id', 'stok_barang.quantity', 'stok_barang.harga')
    	->join('barang', 'barang.id', '=', 'stok_barang.barang_id')
        ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where('stok_barang.delete', 0)
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $supplier;
    }

    public function getFilteredStokBarang($search, $start, $limit, $order, $dir)
    {
        $supplier = $this->select('kategori.name as kategori_name', 'barang.name', 'barang.sku', 'stok_barang.id', 'stok_barang.quantity', 'stok_barang.harga')
        ->join('barang', 'barang.id', '=', 'stok_barang.barang_id')
        ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        ->where(function ($q) use($search) {
            $q->where('stok_barang.id', 'like', '%'.$search.'%');
            $q->orWhere('stok_barang.quantity', 'like', '%'.$search.'%');
            $q->orWhere('stok_barang.harga', 'like', '%'.$search.'%');
            $q->orWhere('barang.name', 'like', '%'.$search.'%');
            $q->orWhere('barang.sku', 'like', '%'.$search.'%');
            $q->orWhere('kategori.name', 'like', '%'.$search.'%');
        })
        ->where('stok_barang.delete', 0)
        ->where('barang.delete', 0)
        ->where('kategori.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $supplier;
    }
}
