<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\SalesModel;

class SalesController extends Controller
{
    private $sales_model;

    public function __construct(SalesModel $sales_model)
    {
        $this->middleware('auth');
        $this->sales_model = $sales_model;
    }

    public function getSales()
    {
        return view('sales', ['user' => Auth::user()]);
    }

    public function getSearchSales(Request $request)
    {
        $sales = $this->sales_model->getFilteredSalesForSelect2($request->input('q'));

        return response()->json(['result' => $sales]);
    }

    public function postAjaxGetSales(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'fullname', 
            2 => 'telp', 
            3 => 'alamat', 
            4 => 'id'
        );
  
        $totalData = $this->sales_model->countAllActiveSales();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $saless = $this->sales_model->getSales($start, $limit, $order, $dir);
        }
        else 
        {
            $saless = $this->sales_model->getFilteredSales($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->sales_model->countAllFilteredActiveSales($search);
        }

        if(!empty($saless))
        {
            foreach ($saless as $sales)
            {
                $edit =  url('/edit-sales/'.$sales->id);
                $delete =  url('/delete-sales/'.$sales->id);

                $nestedData['id'] = $sales->id;
                $nestedData['fullname'] = $sales->fullname;
                $nestedData['telp'] = $sales->telp;
                $nestedData['alamat'] = $sales->alamat;
                $nestedData['btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                    <button onclick='master_delete(\"".$delete."\", \"".'sales'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddSales()
    {
    	return view('add-sales', ['user' => Auth::user()]);
    }

    public function postAddSales()
    {
    	$requested = request()->validate([
    		'fullname' => 'required',
    		'telp' => 'required',
    		'alamat' => 'required'
    	]);

    	$result = $this->sales_model->postAddSales($requested);
    	
    	return redirect()->route('getEditSales', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditSales($id)
    {
    	$sales = $this->sales_model->getOneSales($id);

    	if($sales == null)
    	{
    		return redirect()->route('getSales');
    	}

    	return view('edit-sales', ['user' => Auth::user(), 'sales' => $sales]);
    }

    public function postEditSales($id)
    {
    	$sales = $this->sales_model->getOneSales($id);

    	if($sales == null)
    	{
    		return redirect()->route('getSales');
    	}

    	$requested = request()->validate([
    		'fullname' => 'required',
    		'telp' => 'required',
    		'alamat' => 'required'
    	]);

    	$result = $this->sales_model->postEditSales($requested, $id);

    	return redirect()->route('getEditSales', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteSales($id)
    {
    	$sales = $this->sales_model->getOneSales($id);

    	if($sales == null)
    	{
    		return redirect()->route('getSales');
    	}

    	$result = $this->sales_model->postDeleteSales($id);

    	return json_encode($result);
    }
}
