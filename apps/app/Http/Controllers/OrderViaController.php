<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\OrderViaModel;

class OrderViaController extends Controller
{
    private $order_via_model;

    public function __construct(OrderViaModel $order_via_model)
    {
        $this->middleware('auth');
        $this->order_via_model = $order_via_model;
    }

    public function getOrderVia()
    {
        return view('order-via', ['user' => Auth::user()]);
    }

    public function postAjaxGetOrderVia(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'name', 
            2 => 'id'
        );
  
        $totalData = $this->order_via_model->countAllActiveOrderVia();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $order_vias = $this->order_via_model->getOrderVia($start, $limit, $order, $dir);
        }
        else 
        {
            $order_vias = $this->order_via_model->getFilteredOrderVia($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->order_via_model->countAllFilteredActiveOrderVia($search);
        }

        if(!empty($order_vias))
        {
            foreach ($order_vias as $order_via)
            {
                $edit =  url('/edit-order-via/'.$order_via->id);
                $delete =  url('/delete-order-via/'.$order_via->id);

                $nestedData['id'] = $order_via->id;
                $nestedData['name'] = $order_via->name;
                $nestedData['btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                    <button onclick='master_delete(\"".$delete."\", \"".'order-via'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddOrderVia()
    {
    	return view('add-order-via', ['user' => Auth::user()]);
    }

    public function postAddOrderVia()
    {
    	$requested = request()->validate([
    		'name' => 'required'
    	]);

    	$result = $this->order_via_model->postAddOrderVia($requested);
    	
    	return redirect()->route('getEditOrderVia', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditOrderVia($id)
    {
    	$order_via = $this->order_via_model->getOneOrderVia($id);

    	if($order_via == null)
    	{
    		return redirect()->route('getOrderVia');
    	}

    	return view('edit-order-via', ['user' => Auth::user(), 'order_via' => $order_via]);
    }

    public function postEditOrderVia($id)
    {
    	$order_via = $this->order_via_model->getOneOrderVia($id);

    	if($order_via == null)
    	{
    		return redirect()->route('getOrderVia');
    	}

    	$requested = request()->validate([
    		'name' => 'required'
    	]);

    	$result = $this->order_via_model->postEditOrderVia($requested, $id);

    	return redirect()->route('getEditOrderVia', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteOrderVia($id)
    {
    	$order_via = $this->order_via_model->getOneOrderVia($id);

    	if($order_via == null)
    	{
    		return redirect()->route('getOrderVia');
    	}

    	$result = $this->order_via_model->postDeleteOrderVia($id);

    	return json_encode($result);
    }
}
