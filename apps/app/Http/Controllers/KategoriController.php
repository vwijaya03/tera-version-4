<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\KategoriModel;

class KategoriController extends Controller
{
	private $kategori_model;

    public function __construct(KategoriModel $kategori_model)
    {
        $this->middleware('auth');
        $this->kategori_model = $kategori_model;
    }

    public function getKategori()
    {
        return view('kategori', ['user' => Auth::user()]);
    }

    public function postAjaxGetKategori(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'name',
            2 => 'id'
        );
  
        $totalData = $this->kategori_model->countAllActiveKategori();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $categories = $this->kategori_model->getKategori($start, $limit, $order, $dir);
        }
        else 
        {
            $categories = $this->kategori_model->getFilteredKategori($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->kategori_model->countAllFilteredActiveKategori($search);
        }

        if(!empty($categories))
        {
            foreach ($categories as $category)
            {
                $edit =  url('/edit-kategori/'.$category->id);
                $delete =  url('/delete-kategori/'.$category->id);

                $nestedData['id'] = $category->id;
                $nestedData['name'] = $category->name;
                $nestedData['edit_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                    <button onclick='master_delete(\"".$delete."\", \"".'kategori'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddKategori()
    {
    	return view('add-kategori', ['user' => Auth::user()]);
    }

    public function postAddKategori()
    {
    	$requested = request()->validate([
    		'name' => 'required'
    	]);

    	$result = $this->kategori_model->postAddKategori($requested);
    	
    	return redirect()->route('getEditKategori', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditKategori($id)
    {
    	$kategori = $this->kategori_model->getOneKategori($id);

    	if($kategori == null)
    	{
    		return redirect()->route('getKategori');
    	}

    	return view('edit-kategori', ['user' => Auth::user(), 'kategori' => $kategori]);
    }

    public function postEditKategori($id)
    {
    	$kategori = $this->kategori_model->getOneKategori($id);

    	if($kategori == null)
    	{
    		return redirect()->route('getKategori');
    	}

    	$requested = request()->validate([
    		'name' => 'required'
    	]);

    	$result = $this->kategori_model->postEditKategori($requested, $id);

    	return redirect()->route('getEditKategori', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteKategori($id)
    {
    	$kategori = $this->kategori_model->getOneKategori($id);

    	if($kategori == null)
    	{
    		return redirect()->route('getKategori');
    	}

    	$result = $this->kategori_model->postDeleteKategori($id);

    	return json_encode($result);
    }
}
