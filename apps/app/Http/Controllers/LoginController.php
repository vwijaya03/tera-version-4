<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use Auth, Hash, DB, Log, Carbon;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getLogin', 'postLogin']]);
    }

    public function getLogin()
    {
        if(Auth::check())
        {
            return redirect()->route('getDashboard');
        }

        return view('login');
    }

    public function postLogin()
    {
        $requested = request()->validate([
    		'username' => 'required',
    		'password' => 'required'
    	]);

        if(Auth::attempt(['username' => $requested['username'], 'password' => $requested['password']]))
        {
            return redirect()->route('getDashboard');
        }
        else if(!Auth::attempt(['username' => $requested['username'], 'password' => $requested['password']]))
        {
            return redirect()->route('getLogin')->with(['err' => 'Username atau password salah.']);
        }
        else
        {
            return redirect()->route('getLogin')->with(['err' => 'Akses ditolak.']);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }
}
