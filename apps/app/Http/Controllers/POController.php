<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\PurchaseOrderModel;
use App\ItemPurchaseOrderModel;
use App\TmpItemPurchaseOrderModel;

class POController extends Controller
{
    private $po_model;
    private $item_po_model;
    private $tmp_item_po_model;

    public function __construct(PurchaseOrderModel $po_model, ItemPurchaseOrderModel $item_po_model, TmpItemPurchaseOrderModel $tmp_item_po_model)
    {
        $this->middleware('auth');
        $this->po_model = $po_model;
        $this->item_po_model = $item_po_model;
        $this->tmp_item_po_model = $tmp_item_po_model;
    }

    public function getPO()
    {
        $search_supplier_url = url('/search-supplier');
        $search_sales_url = url('/search-sales');

    	return view('po', ['user' => Auth::user(), 'supplier_url' => $search_supplier_url, 'sales_url' => $search_sales_url]);
    }

    public function postPO()
    {
        $requested = request()->validate([
            'supplier' => 'required'
        ]);

        print_r($requested);
        die();
    }
}
