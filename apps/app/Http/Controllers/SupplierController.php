<?php

namespace App\Http\Controllers;
use Auth, Hash, DB, Log;
use App\SupplierModel;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    private $supplier_model;

    public function __construct(SupplierModel $supplier_model)
    {
        $this->middleware('auth');
        $this->supplier_model = $supplier_model;
    }

    public function getSupplier()
    {
        return view('supplier', ['user' => Auth::user()]);
    }

    public function getSearchSupplier(Request $request)
    {
        $suppliers = $this->supplier_model->getFilteredSupplierForSelect2($request->input('q'));

        return response()->json(['result' => $suppliers]);
    }

    public function postAjaxGetSupplier(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'fullname', 
            2 => 'email', 
            3 => 'telp', 
            4 => 'alamat', 
            5 => 'termin', 
            6 => 'id'
        );
  
        $totalData = $this->supplier_model->countAllActiveSupplier();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $suppliers = $this->supplier_model->getSupplier($start, $limit, $order, $dir);
        }
        else 
        {
            $suppliers = $this->supplier_model->getFilteredSupplier($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->supplier_model->countAllFilteredActiveSupplier($search);
        }

        if(!empty($suppliers))
        {
            foreach ($suppliers as $supplier)
            {
                $edit =  url('/edit-supplier/'.$supplier->id);
                $delete =  url('/delete-supplier/'.$supplier->id);

                $nestedData['id'] = $supplier->id;
                $nestedData['fullname'] = $supplier->fullname;
                $nestedData['email'] = $supplier->email;
                $nestedData['telp'] = $supplier->telp;
                $nestedData['alamat'] = $supplier->alamat;
                $nestedData['termin'] = $supplier->termin." Hari";
                $nestedData['btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                    <button onclick='master_delete(\"".$delete."\", \"".'supplier'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddSupplier()
    {
    	return view('add-supplier', ['user' => Auth::user()]);
    }

    public function postAddSupplier()
    {
    	$requested = request()->validate([
    		'fullname' => 'required',
    		'email' => 'required',
    		'telp' => 'required',
    		'alamat' => 'required',
    		'termin' => 'required'
    	]);

    	$result = $this->supplier_model->postAddSupplier($requested);
    	
    	return redirect()->route('getEditSupplier', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditSupplier($id)
    {
    	$supplier = $this->supplier_model->getOneSupplier($id);

    	if($supplier == null)
    	{
    		return redirect()->route('getSupplier');
    	}

    	return view('edit-supplier', ['user' => Auth::user(), 'supplier' => $supplier]);
    }

    public function postEditSupplier($id)
    {
    	$supplier = $this->supplier_model->getOneSupplier($id);

    	if($supplier == null)
    	{
    		return redirect()->route('getSupplier');
    	}

    	$requested = request()->validate([
    		'fullname' => 'required',
    		'email' => 'required',
    		'telp' => 'required',
    		'alamat' => 'required',
    		'termin' => 'required'
    	]);

    	$result = $this->supplier_model->postEditSupplier($requested, $id);

    	return redirect()->route('getEditSupplier', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteSupplier($id)
    {
    	$supplier = $this->supplier_model->getOneSupplier($id);

    	if($supplier == null)
    	{
    		return redirect()->route('getSupplier');
    	}

    	$result = $this->supplier_model->postDeleteSupplier($id);

    	return json_encode($result);
    }
}
