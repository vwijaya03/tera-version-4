<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\KategoriModel;
use App\BarangModel;
use App\StokBarangModel;

class BarangController extends Controller
{
    private $barang_model;
    private $stok_barang_model;

    public function __construct(BarangModel $barang_model, StokBarangModel $stok_barang_model)
    {
        $this->middleware('auth');
        $this->barang_model = $barang_model;
        $this->stok_barang_model = $stok_barang_model;
    }

    public function getStokBarang()
    {
        return view('stok-barang', ['user' => Auth::user()]);
    }

    public function getBarang()
    {
        return view('barang', ['user' => Auth::user()]);
    }

    public function postAjaxGetStokBarang(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'stok_barang.id', 
            1 => 'kategori.name', 
            2 => 'barang.name',
            3 => 'barang.sku',
            4 => 'stok_barang.quantity',
            5 => 'stok_barang.harga'
        );
  
        $totalData = $this->stok_barang_model->countAllActiveStokBarang();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $stoks = $this->stok_barang_model->getStokBarang($start, $limit, $order, $dir);
        }
        else 
        {
            $stoks = $this->stok_barang_model->getFilteredStokBarang($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->stok_barang_model->countAllFilteredActiveStokBarang($search);
        }

        if(!empty($stoks))
        {
            foreach ($stoks as $stok)
            {
                $edit =  url('/edit-barang/'.$stok->barang_id);
                $delete =  url('/delete-barang/'.$stok->barang_id);

                $nestedData['id'] = $stok->id;
                $nestedData['kategori'] = $stok->kategori_name;
                $nestedData['barang_name'] = $stok->name;
                $nestedData['barang_sku'] = $stok->sku;
                $nestedData['quantity'] = $stok->quantity;
                $nestedData['harga'] = $stok->harga;
                // $nestedData['edit_btn'] = "
                //     <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                //     <button onclick='master_delete(\"".$delete."\", \"".'stok-barang'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                // ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAjaxGetBarang(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'barang.id', 
            1 => 'barang.sku', 
            2 => 'kategori.name',
            3 => 'barang.name',
            4 => 'barang.id'
        );
  
        $totalData = $this->barang_model->countAllActiveBarang();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $products = $this->barang_model->getBarang($start, $limit, $order, $dir);
        }
        else 
        {
            $products = $this->barang_model->getFilteredBarang($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->barang_model->countAllFilteredActiveBarang($search);
        }

        if(!empty($products))
        {
            foreach ($products as $product)
            {
                $edit =  url('/edit-barang/'.$product->barang_id);
                $delete =  url('/delete-barang/'.$product->barang_id);

                $nestedData['id'] = $product->barang_id;
                $nestedData['sku'] = $product->barang_sku;
                $nestedData['kategori'] = $product->kategori_name;
                $nestedData['nama_barang'] = $product->barang_name;
                $nestedData['edit_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i>Ubah</button>
                    <button onclick='master_delete(\"".$delete."\", \"".'barang'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i>Hapus</button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddBarang()
    {
    	$categories = $this->barang_model->getAllKategori();

    	return view('add-barang', ['user' => Auth::user(), 'categories' => $categories]);
    }

    public function postAddBarang()
    {
    	$requested = request()->validate([
    		'kategori' => 'required',
    		'sku' => 'required',
    		'name' => 'required'
    	]);

    	$result = $this->barang_model->postAddBarang($requested);
    	
    	return redirect()->route('getEditBarang', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditBarang($id)
    {
    	$barang = $this->barang_model->getOneBarang($id);

    	if($barang == null)
    	{
    		return redirect()->route('getBarang');
    	}

    	$selected_category = $this->barang_model->getSelectedKategori($barang->barang_kategori_id);
    	$unselected_categories = $this->barang_model->getUnSelectedKategori($barang->barang_kategori_id);

    	return view('edit-barang', ['user' => Auth::user(), 'barang' => $barang, 'selected_category' => $selected_category, 'unselected_categories' => $unselected_categories]);
    }

    public function postEditBarang($id)
    {
    	$barang = $this->barang_model->getOneBarang($id);

    	if($barang == null)
    	{
    		return redirect()->route('getBarang');
    	}

    	$requested = request()->validate([
    		'kategori' => 'required',
    		'sku' => 'required',
    		'name' => 'required'
    	]);

    	$result = $this->barang_model->postEditBarang($requested, $id);

    	return redirect()->route('getEditBarang', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteBarang($id)
    {
    	$barang = $this->barang_model->getOneBarang($id);

    	if($barang == null)
    	{
    		return redirect()->route('getBarang');
    	}

    	$result = $this->barang_model->postDeleteBarang($id);

    	return json_encode($result);
    }
}
