<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log, Carbon;
use RuntimeException;
use Goutte\Client;
use App\KategoriModel;

class DashboardController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getDashboard()
    {
        return view('dashboard', ['user' => Auth::user()]);
    }

    public function getRecentReleaseAnime()
    {
    	$base_url = 'https://ww3.gogoanime.io';
    	$page = 1;
    	$plot_summary = "Plot Summary: ";
    	$plot_summary_len = strlen($plot_summary);

    	$released = "Released: ";
    	$released_len = strlen($released);

    	$ongoing_status = "Status: ";
    	$ongoing_status_len = strlen($ongoing_status);

    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$url = $base_url.'/page-recent-release.html?page='.$page;

    	$recent_release_client = new Client();

    	$crawler_recent_release = $recent_release_client->request('GET', $url);

    	$crawler_recent_release->filter('.items .name a')->each(function ($node, $i) use($crawler_recent_release, $recent_release_client, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string) {
		    
    		$anime_detail_link = $crawler_recent_release->selectLink(trim($node->text()))->link();
			$crawler_anime_detail = $recent_release_client->click($anime_detail_link);

			// Title
			$title = $crawler_anime_detail->filter('.title_name h2')->each(function ($node) {
				print $node->text();
			});

			print_r($title[0]);

			print("\n");

			// Category
			$category = $crawler_anime_detail->filter('.anime_video_body_cate a')->attr('title');
			print_r($category);

			// Category Slug
			$category_slug = substr($crawler_anime_detail->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
			print_r($category_slug);

			print "\n";

			// Anime Detail Slug
			$anime_detail_slug = substr($crawler_anime_detail->filter('head link[rel=canonical]')->attr('href'), 1);
			print_r($anime_detail_slug);

			print "\n";

			// Anime Info
			$anime_info = $crawler_anime_detail->filter('.anime-info a')->attr('title');
			print_r($anime_info);

			print "\n";

			// Anime Info Slug
			$anime_info_slug = substr($crawler_anime_detail->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
			print_r($anime_info_slug);

			print "\n";

			// Streaming Links
			$streaming_links = $crawler_anime_detail->filter('a[data-video]')->extract(array('data-video'));
			print_r($streaming_links);

			// Streaming Link Names
			$streaming_link_names = $crawler_anime_detail->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
			    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
			});
			print_r($streaming_link_names);

			// Cari link dengan nama tombol download, lalu di pencet
			$link = $crawler_anime_detail->selectLink('Download')->link();
			$crawler_download = $recent_release_client->click($link);

			// Download Links
			$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

			print_r($download_links);

			print "\n";

			// Download Link Names
			$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
			    return $node->text();
			});

			print_r($download_text_names);

			print("\n");
			print("\n");
			print("\n");
		});
    }

    public function getAnime()
    {
    	$base_url = 'https://ww3.gogoanime.io';
    	$plot_summary = "Plot Summary: ";
    	$plot_summary_len = strlen($plot_summary);

    	$released = "Released: ";
    	$released_len = strlen($released);

    	$ongoing_status = "Status: ";
    	$ongoing_status_len = strlen($ongoing_status);

    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$url = $base_url.'/category/naruto-shippuuden-movie-1';

    	$client = new Client();

    	$crawler = $client->request('GET', $url);

    	// Nge get cuma 1 biji
    	$anime_slug = substr($crawler->filter('head link[rel=canonical]')->attr('href'), $remove_category_string_for_slug);
		print_r("Slug: ".$anime_slug);

		print "\n";

		$anime_image = $crawler->filter('.anime_info_body_bg img')->attr('src');
		print_r($anime_image);

		print "\n";

		$anime_title = $crawler->filter('.anime_info_body_bg h1')->each(function ($node) {
		    return $node->text();
		});

		print($anime_title[0]);

		print "\n";

		$anime_sub_category = $crawler->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
		    $node->text();
		});

		print($anime_sub_category[0]);

		print "\n";

		$anime_description = $crawler->filter('.anime_info_body_bg p')->eq(2)->each(function ($node) use ($plot_summary_len) {
			print substr($node->text(), $plot_summary_len);
		});

		print($anime_description[0]);

		print "\n";

		$anime_genre_texts = $crawler->filter('.anime_info_body_bg a')->extract(array('title'));
		// print_r($crawler->filter('.anime_info_body_bg a')->extract(array('title')));

		unset($anime_genre_texts[0]);
		unset($anime_genre_texts[1]);

		print_r($anime_genre_texts);
		print "\n";

		foreach ($anime_genre_texts as $anime_genre_text) 
		{
			print($anime_genre_text);
			print "\n";
			print(str_slug($anime_genre_text, '-'));
			print "\n";
		}

		$anime_released = $crawler->filter('.anime_info_body_bg p')->eq(4)->each(function ($node) use ($released_len) {
			return substr($node->text(), $released_len);
		});

		print($anime_released[0]);

		print "\n";

		$anime_status = $crawler->filter('.anime_info_body_bg p')->eq(5)->each(function ($node) use ($ongoing_status_len) {
			return substr($node->text(), $ongoing_status_len);
		});

		print($anime_status[0]);

		print "\n";
		print "\n";
		print "\n";

		$ep_start = $crawler->filter('#episode_page a.active')->attr('ep_start');
		$ep_end = $crawler->filter('#episode_page a.active')->attr('ep_end');
		$movie_id = $crawler->filter('input#movie_id')->attr('value');

		print($ep_start. "\n");
		print($ep_end. "\n");
		print($movie_id. "\n");

		$get_load_list_episode_url = $base_url.'/load-list-episode?ep_start='.$ep_start.'&ep_end='.$ep_end.'&id='.$movie_id.'';

		$get_episode_lists = new Client();

    	$crawler_episode_lists = $get_episode_lists->request('GET', $get_load_list_episode_url);

    	$crawler_episode_lists->filter('li .name')->each(function ($node, $i) use ($crawler_episode_lists, $get_episode_lists, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string) {

			$episode_links = $crawler_episode_lists->selectLink(trim($node->text()))->link();
			$crawler_episode_links = $get_episode_lists->click($episode_links);

			// Title
			$title = $crawler_episode_links->filter('.title_name h2')->each(function ($node) {
				print $node->text();
			});

			print_r($title[0]);

			print("\n");

			// Category
			$category = $crawler_episode_links->filter('.anime_video_body_cate a')->attr('title');
			print_r($category);

			// Category Slug
			$category_slug = substr($crawler_episode_links->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
			print_r($category_slug);

			print "\n";

			// Anime Detail Slug
			$anime_detail_slug = substr($crawler_episode_links->filter('head link[rel=canonical]')->attr('href'), 1);
			print_r($anime_detail_slug);

			print "\n";

			// Anime Info
			$anime_info = $crawler_episode_links->filter('.anime-info a')->attr('title');
			print_r($anime_info);

			print "\n";

			// Anime Info Slug
			$anime_info_slug = substr($crawler_episode_links->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
			print_r($anime_info_slug);

			print "\n";

			// Streaming Links
			$streaming_links = $crawler_episode_links->filter('a[data-video]')->extract(array('data-video'));
			print_r($streaming_links);

			// Streaming Link Names
			$streaming_link_names = $crawler_episode_links->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
			    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
			});
			print_r($streaming_link_names);

			if(count($streaming_links) == 0) 
			{
				Log::info('url link kosong');
			}

			if(count($streaming_link_names) == 0) 
			{
				Log::info('nama link kosong');
			}

			if(count($streaming_link_names) != count($streaming_links)) 
			{
				Log::info('total url dan nama nya tidak sama');
			}
			else
			{
				Log::info('count nama link: '.count($streaming_link_names));
				Log::info('count url link: '.count($streaming_links));

				for ($i=0; $i < count($streaming_links); $i++) { 

				    $findme   = 'https:';
				    $pos = strpos($streaming_links[$i], $findme);

				    // Note our use of ===. Simply, == would not work as expected
				    // because the position of 'a' was the 0th (first) character.
				    if ($pos === false) {
				        $streaming_links[$i] = 'https:'.$streaming_links[$i];
				    }
					Log::info($streaming_links[$i]);
				}
			}

			// Cari link dengan nama tombol download, lalu di pencet
			$link = $crawler_episode_links->selectLink('Download')->link();
			$crawler_download = $get_episode_lists->click($link);

			// Download Links
			$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

			print_r($download_links);

			print "\n";

			// Download Link Names
			$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
			    return $node->text();
			});

			print_r($download_text_names);

			print("\n");
			print("\n");
			print("\n");
		});

    	// https://ww3.gogoanime.io/load-list-episode?ep_start=2&ep_end=5&id=133

    	die();
    }

    public function getDetailAnime()
    {
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$client = new Client();

    	$crawler = $client->request('GET', 'https://ww3.gogoanime.io/black-clover-tv-episode-5');

    	// Title
		$title = $crawler->filter('.title_name h2')->each(function ($node) {
			return $node->text();
		});

		print_r($title[0]);

		print "\n";

		// Category
		$category = $crawler->filter('.anime_video_body_cate a')->attr('title');
		print_r($category);

		print "\n";

		// Category Slug
		$category_slug = substr($crawler->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
		print_r($category_slug);

		print "\n";

		// Anime Detail Slug
		$anime_detail_slug = substr($crawler->filter('head link[rel=canonical]')->attr('href'), 1);
		print_r($anime_detail_slug);

		print "\n";

		// Anime Info
		$anime_info = $crawler->filter('.anime-info a')->attr('title');
		print_r($anime_info);

		print "\n";

		// Anime Info Slug
		$anime_info_slug = substr($crawler->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
		print_r($anime_info_slug);

		print "\n";

		// Streaming Links
		$streaming_links = $crawler->filter('a[data-video]')->extract(array('data-video'));
		print_r($streaming_links);

		// Streaming Link Names
		$streaming_link_names = $crawler->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
		    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
		});
		print_r($streaming_link_names);

		// Cari link dengan nama tombol download, lalu di pencet
		$link = $crawler->selectLink('Download')->link();
		$crawler_download = $client->click($link);

		// Download Links
		$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

		print_r($download_links);

		print "\n";

		// Download Link Names
		$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
		    return $node->text();
		});

		print_r($download_text_names);
		// foreach ($download_text_names as $download_text_name) 
		// {
		// 	print($download_text_name);
		// 	print "\n";
		// }

    }

    public function penjelasan_fungsi_dom_crawler()
    {
    	// filter dengan class nya dan html tag h2, lalu ambil value dari h2 nya
		$title = $crawler->filter('.title_name h2')->each(function ($node) {
		    print "Title: ".$node->text()."\n";
			return $node->text();
		});

    	// Nge get cuma 1 biji, filter class nya lalu ambil attribute value dengan yang ada title nya
		$anime_info = $crawler->filter('.anime-info a')->attr('title');
		print_r("Anime Info: ".$anime_info);

		// Ambil array ke 1 dari result
		$crawler->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
		    print $node->text()."\n";
		});

		// hasil result array dari isi genre, lalu array ke 0 dan 1 di buang, lalu di foreach array nya
		$isi_genre = $crawler->filter('.anime_info_body_bg a')->extract(array('title'));
		// print_r($crawler->filter('.anime_info_body_bg a')->extract(array('title')));

		unset($isi_genre[0]);
		unset($isi_genre[1]);

		print_r($isi_genre);
		print "\n";

		foreach ($isi_genre as $genre) 
		{
			print($genre);
			print "\n";
		}
    }
}
